<?php

require "BDD/bddconfig.php";

$paramOK = false;
if ((isset($_POST['pseudo']))) {
    $pseudo = htmlspecialchars($_POST['pseudo']);
    $paramOK = true;
}
if ((isset($_POST['message']))) {
    $message = htmlspecialchars($_POST['message']);
    $paramOK = true;
}

if ($paramOK = true) {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $pdoStmt = $objBdd->prepare("INSERT INTO messages (message, pseudo) VALUES (:message, :pseudo)");
    $pdoStmt->bindParam(':pseudo', $pseudo, PDO::PARAM_STR);
    $pdoStmt->bindParam(':message', $message, PDO::PARAM_STR);
    $pdoStmt->execute();

    $lastId = $objBdd->lastInsertId();
    echo $lastId;



    //$serveur = $_SERVER['HTTP_POST'];
    //$chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    //$page = 'index.php';

    header("Location: http://localhost/livreOr/index.php");
}
