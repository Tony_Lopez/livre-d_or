<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Livre d'or</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div id="titre1">
        <h1>Livre d'or</h1>
    </div>

    <div id="formulaire">

        <form method="POST" action="formulaire.php">
            <label for="pseudo">PSEUDO</label>
            <input type="text" name="pseudo" />

            <label for="message">MESSAGE</label>
            <textarea name="message" id="message" cols="70" rows="10"></textarea>

            <input type="submit" value="ENVOYER" />
        </form>

    </div>

    <?php
    require "BDD/bddconfig.php";
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $listeMessage = $objBdd->prepare("SELECT * FROM messages ORDER BY date DESC");
    $listeMessage->execute();
    ?>
    <?php foreach ($listeMessage as $msg) { ?>

        <p><?php echo $msg['pseudo']; ?></p>
        <p><?php echo $msg['message']; ?></p>
        <p><?php echo $msg['date']; ?></p>

    <?php }
    $listeMessage->closeCursor();
    ?>
</body>

</html>